const express = require("express");
const cors = require('cors')
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const app = express();

const port = process.env.PORT || 8080;

// CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    app.use(cors());
    next();
});

// SWAGGER
const swaggerDefinition = {
  "swagger": "2.0",
  "info": {
    "description": "BrainPay API",
    "version": "2021-06-23T20:16:59Z",
    "title": "BrainPay API"
  },
  "externalDocs": {
    "description": "BrainPay Website",
    "url": "https://www.brainpay.com.br"
  },
  "host": "service.brainpay.com.br",
  "basePath": "/test",
  "tags": [
    {
      "name": "PaymentsNotice",
      "description": "Withdrawal related operations"
    },
    {
      "name": "RechargeNotice",
      "description": "Recharge related operations"
    },
    {
      "name": "Statement",
      "description": "Status of a recharge and withdrawal transaction."
    },
    {
      "name": "CompanyAuth",
      "description": "Companies authentication methods"
    },
    {
      "name": "KYC",
      "description": "KYC related services"
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/v1/deposit/url/{id_card}": {
      "get": {
        "tags": [
          "RechargeNotice"
        ],
        "summary": "Generate URL to PIX",
        "description": "<strong>Important:</strong> In order to perform this transaction, the user's CPF must be previously registered on our KYC service.<br>\r\n This service provides a temporary deposit URL. If you want to use the BrainPay interface, use our API as below:  <br>\r\n With the token generated in the previous step <strong> CompanyAuth / Login to the service </strong> and the user's CPF's (id_card) is possible to \r\n generate a temporary link with a lifetime of 15 minutes where the user can make a deposit to your company. <br>\r\n In the response, besides the link, the transaction_id number will also be sent so that it is possible to identify the payment made in \r\n the next step <strong>RechargeNotice / Retrieve a Rechange Staus</strong>. <br>\r\n It is possible to indicate the internal company control number. For this, enter it as a querystring \r\n (v1/deposit/url/&#60;cpf&#62;/?transaction_id=&#60;company control number&#62;), if you do not have this number, our software will create it automatically.\r\n  In this case, there is no need to indicate this querystring:(v1/deposit/url/&#60;cpf&#62;)<strong> <br>\r\nNote:</strong> See endpoint <strong>/v1/deposit/url/</strong> for other deposit options.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "id_card",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "transaction_id",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/UrlDepositSuccess200"
            }
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/UrlDepositSuccess201"
            }
          },
          "404": {
            "description": "404 response",
            "schema": {
              "$ref": "#/definitions/UrlDepositNotFound"
            }
          },
          "409": {
            "description": "409 response",
            "schema": {
              "$ref": "#/definitions/UrlDepositError"
            }
          }
        }
      },
    },
    "/v1/transaction/history": {
      "get": {
        "tags": [
          "Statement"
        ],
        "description": "This service provides the status of a recharge and withdrawal transaction. <br>\r\n  In the querystring, enter the start date and end date, both are timestamp.<br>\r\n<strong>Note:</strong> The &#34;order_id&#34; field is our internal control field, &#34;timestamp&#34; refers to the transaction creation date, transaction_id&#34; is the transaction identification \r\n number, while &#34;type&#34; indicates whether the transaction is a deposit (payin)  or withdrawal (payout).",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "start_time",
            "in": "query",
            "required": true,
            "type": "string"
          },
          {
            "name": "end_time",
            "in": "query",
            "required": true,
            "type": "string"
          },
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string"
          },
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/TransactionHistorySuccess200"
            },
          },
          "406": {
            "description": "406 response",
            "schema": {
              "$ref": "#/definitions/TransactionHistoryNotAcceptable"
            }
          }
        }
      }
      },
    "/v1/auth/company/account": {
      "post": {
        "tags": [
          "CompanyAuth"
        ],
        "summary": "Create a new account",
        "description": "In order to make requests to the other services, you need first create a user. <br>\r\n After successfully creating a user, ask a BrainPay staff member to activate your account.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "Login",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Login"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/CreateUserSuccess200"
            },
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/CreateUserSuccess201"
            }
          },
          "401": {
            "description": "401 response",
            "schema": {
              "$ref": "#/definitions/CreateUserUnauthorized"
            }
          },
          "406": {
            "description": "406 response",
            "schema": {
              "$ref": "#/definitions/CreateUserNotAcceptable"
            }
          },
          "409": {
            "description": "409 response",
            "schema": {
              "$ref": "#/definitions/CreateUserError"
            }
          }
        }
      },
    },
    "/v1/auth/company/login": {
      "post": {
        "tags": [
          "CompanyAuth"
        ],
        "summary": "Login to the service",
        "description": "Once your user is activated, you are able to ask for a token to make requests to the other BrainPay services. \r\n A token is always issued with a lifetime limit (currently, it is 15 minutes). \r\n<br>Every time your token expires, your client needs to ask for another by doing login with your user and password \r\ncreated in the previous step.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "Login",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Login"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/LoginSuccess200"
            },
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/LoginSuccess201"
            }
          },
          "401": {
            "description": "401 response",
            "schema": {
              "$ref": "#/definitions/LoginUnauthorized"
            }
          },
          "404": {
            "description": "404 response",
            "schema": {
              "$ref": "#/definitions/LoginNotFound"
            }
          },
          "409": {
            "description": "409 response",
            "schema": {
              "$ref": "#/definitions/LoginError"
            }
          }
        }
      },
    },
    "/v1/deposit/status": {
      "post": {
        "tags": [
          "RechargeNotice"
        ],
        "summary": "Retrieve a recharge status",
        "description": "This service provides the status of a recharge transaction. It is possible to check up to 100 transactions in a single request.\r\n<br> The status of transactions will be sent by webhook.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "DataList",
            "required": true,
            "schema": {
              "$ref": "#/definitions/DataList"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/DataListSuccess200"
            },
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/DataListSuccess201"
            }
          },
          "406": {
            "description": "406 response",
            "schema": {
              "$ref": "#/definitions/DataListNotAcceptable"
            }
          },
          "409": {
            "description": "409 response",
            "schema": {
              "$ref": "#/definitions/DataListError"
            }
          }
        }
      },
    },
    "/v1/deposit/url": {
      "post": {
        "tags": [
          "RechargeNotice"
        ],
        "summary": "Generate URL to PIX",
        "description": "<strong>Important:</strong> In order to perform this transaction, the user's CPF must be previously registered on our KYC service.<br>\r\n This service provides a temporary deposit URL. If you want to use the BrainPay interface, use our API as below: <br>\r\n The goal of this endpoint is the same as the endpoint <strong>/v1/deposit/url/v1/deposit/url/{id_card}</strong>, the difference is that it is possible \r\n in 'transaction_id' to pass some information for your internal control and in 'redirect_url' (Not required field) adding an url to where \r\n the user will be redirected after \r\n the deposit. If you do not have the order control number, indicate an empty string in transaction_id and our system will automatically \r\n create this number.<br>\r\n With the token generated in the previous step <strong>CompanyAuth / Login to the service </strong> and the user's CPF's (id_card) is possible to \r\n generate a temporary link with a lifetime of 15 minutes where the user can make a deposit to your company.<br>\r\n In the response, besides the link, the transaction_id number will also be sent so that it is possible to identify the payment made \r\n in the next step <strong>RechargeNotice / Retrieve a Rechange Staus</strong>.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "DepositUrlGenerator",
            "required": true,
            "schema": {
              "$ref": "#/definitions/DepositUrlGenerator"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/UrlDepositSuccess200"
            },
            "headers": {
              "Access-Control-Allow-Origin": {
                "type": "string"
              }
            }
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/UrlDepositSuccess201"
            }
          },
          "404": {
            "description": "404 response",
            "schema": {
              "$ref": "#/definitions/UrlDepositNotFound"
            }
          },
          "409": {
            "description": "409 response",
            "schema": {
              "$ref": "#/definitions/UrlDepositError"
            }
          }
        }
      }
      },
    "/v1/kyc/cep/{zip_number}": {
      "get": {
        "tags": [
          "KYC"
        ],
        "summary": "Get an address given its zip number",
        "description": "The CEP (Zip Code) is a postal code for Brazil. It identifies all the details of the address.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "zip_number",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/CEPSuccess200"
            },
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/CEPSuccess201"
            }
          },
          "404": {
            "description": "404 response",
            "schema": {
              "$ref": "#/definitions/CEPNotFound"
            }
          },
          "409": {
            "description": "409 response",
            "schema": {
              "$ref": "#/definitions/CEPError"
            }
          }
        }
      },
    },
    "/v1/kyc/cpf": {
      "post": {
        "tags": [
          "KYC"
        ],
        "summary": "Register user's CPF number",
        "description": "CPF is the Brazilian ID card for citizens. It’s an obligatory, unique number.\r\n<br> The user registration is done even if the CPF is not in Brazil Federal Government database. We receive the CPF, register it with \r\n general information, and later we register it according to Brazil Federal Government data. This is a Asynchronous Response. \r\n<br> We also offer a synchronous API that communicates with the Brazil Federal Government database.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "CPF",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CPF"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/CPFPostSuccess200"
            }
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/CPFPostSuccess201"
            }
          },
          "406": {
            "description": "406 response",
            "schema": {
              "$ref": "#/definitions/CPFPostNotAcceptable"
            }
          },
          "407": {
            "description": "407 response",
            "schema": {
              "$ref": "#/definitions/CPFPostNotAuthentication"
            }
          },
          "409": {
            "description": "409 response",
            "schema": {
              "$ref": "#/definitions/CPFPostError"
            }
          }
        }
      },
    },
    "/v1/kyc/cpf/{cpf-search}": {
      "get": {
        "tags": [
          "KYC"
        ],
        "summary": "Search for user's CPF number registered",
        "description":"CPF is the Brazilian ID card for citizens. It’s an obligatory, unique number. Send the user's CPF to get data from the user \r\n that has already been registered in Register User method. \r\n<br>This is a Asynchronous Response. If the user has not been registered, this method will register this user.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "cpf-search",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/CPFGetSuccess200"
            },
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/CPFGetSuccess201"
            }
          },
          "406": {
            "description": "406 response",
            "schema": {
              "$ref": "#/definitions/CPFGetNotAcceptable"
            }
          },
          "407": {
            "description": "407 response",
            "schema": {
              "$ref": "#/definitions/CPFGetNotAuthentication"
            }
          },
          "409": {
            "description": "409 response",
            "schema": {
              "$ref": "#/definitions/CPFGetError"
            }
          }
        }
      },
    },
    "/v1/withdrawal/banks": {
      "get": {
        "tags": [
          "PaymentsNotice"
        ],
        "summary": "List brazilian banks",
        "description":"Get the list of brazilian banks.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/BanksSuccess200"
            },
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/BanksSuccess201"
            }
          },
          "409": {
            "description": "409 response",
            "schema": {
              "$ref": "#/definitions/BanksError"
            }
          }
        }
      },
    },
    "/v1/withdrawal/ted": {
      "post": {
        "tags": [
          "PaymentsNotice"
        ],
        "summary": "TED",
        "description": "<strong>Important:</strong> In order to perform this transaction, the user's CPF must be previously registered on our KYC service.<br>\r\n Whenever your company needs to send money over to another bank account, you will need to use this service. This service is \r\n useful for paying out your customers. In order to provide information to this service, your client needs to inform his/her bank \r\n data as required below. \r\n<br> Our payout method is asynchronous. It means that your request will be received by our servers and be processed in a queue. \r\n All the statuses of the process will be sent to your company through a webhook your technical team needs to build according \r\n to our specifications. \r\n<br>A TED payout flow is usually finished in up to 30 minutes if the order is requested within the Brazilian banking system working \r\n time (Monday to Friday, 8 am/5 pm, Brazil TZ). Out of this range of time, it will only be concluded in the next workday.\r\n<br> <strong>Note:</strong> The last two decimal places of the amount field are considered decimal. If you enter 123 it will be converted to 1.23.\r\nIn the test environment it is only possible to create TEDs with values below R$1,000.00 and the statuses you will receive in \r\n the webhook are:\r\n<br><strong>Accepted</strong> = 0 \r\n<br><strong>Processing</strong> = 1 \r\n<br><strong>Approved</strong> = 2 \r\n<br><strong>Rejected</strong> = 3 \r\n<br><strong>Insufficient Balance</strong> = 20 \r\n<br><strong>Too Low Amount</strong> = 21 \r\n<br><strong>Bank Offline</strong> = 22 \r\n<br><strong>KYC Needed</strong> = 23 \r\n<br>",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "Authorization",
            "in": "header",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "TED",
            "required": true,
            "schema": {
              "$ref": "#/definitions/TED"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/TEDSuccess200"
            },
          },
          "201": {
            "description": "201 response",
            "schema": {
              "$ref": "#/definitions/TEDSuccess201"
            },
          },
          "406": {
            "description": "406 response",
            "schema": {
              "$ref": "#/definitions/TEDNotAcceptable"
            }
          }
        }
      },
    }
  },
  "definitions": {
    "TransactionHistoryNotAcceptable": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 406,
        },
        "message": {
          "type": "string",
          "example": "Error description",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "TransactionHistoryNotAcceptable",
      "description": "Not Acceptable"
    },
    "TransactionHistorySuccess200": {
      "type": "object",
      "required": [
        "code",
        "status",
        "transactions"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 200
        },
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "transactions": {
          "type": "array",
          "items": {
            "type": "object",
            "example": [ { 
                "order_id": 1783,
                "timestamp": 1624993770,
                "status": "Processing",
                "transaction_id": "7895",
                "amount": "16.00",
                "type": "payin"
              },
              { 
                "order_id": 1467,
                "timestamp": 1624554285,
                "status": "Approved",
                "transaction_id": "24387da9-b9be-42ee-921a-0c3bc0fc68df",
                "amount": "200.00",
                "type": "payout"
            },
          ],
            "properties": {}
          }
        }
      },
      "title": "TransactionHistorySuccess200",
      "description": "Success"
    },

    "DepositUrlGenerator": {
      "type": "object",
      "required": [
        "amount",
        "id_card",
      ],
      "properties": {
        "id_card": {
          "type": "string",
          "example": "05998217608",
          "minLength": 11,
          "maxLength": 11
        },
        "amount": {
          "type": "integer",
          "example": "100",
          "minimum": 100,
          "maximum": 500000,
          "exclusiveMaximum": true
        },
        "transaction_id": {
          "type": "string",
          "example": "123456",
          "maxLength": 32
        },
        "redirect_url": {
          "type": "string",
          "example": "www.mycompany.com",
          "maxLength": 255
        }
      },
      "title": "DepositUrlGenerator"
    },
    "UrlDepositNotFound": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Not found",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 404,
        },
        "message": {
          "type": "string",
          "example": "User does not exist",
          "minLength": 100,
          "maxLength": 8
        }
      },
      "title": "UrlDepositNotFound",
      "description": "Not Found"
    },
    "Empty": {
      "type": "object",
      "title": "empty-schema"
    },
    "UrlDepositSuccess201": {
      "type": "object",
      "required": [
        "code",
        "status",
        "transaction_id",
        "url"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 201
        },
        "transaction_id": {
          "type": "string",
          "example": "e7c2ca9f-d9b1-4349-a57d-1444ecdd7219",
          "minLength": 4,
          "maxLength": 36
        },
        "url": {
          "type": "string",
          "example": "url",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "UrlDepositSuccess201",
      "description": "Success"
    },
    "UrlDepositSuccess200": {
      "type": "object",
      "required": [
        "code",
        "status",
        "transaction_id",
        "url"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 200,
        },
        "transaction_id": {
          "type": "string",
          "example": "e7c2ca9f-d9b1-4349-a57d-1444ecdd7219",
          "minLength": 4,
          "maxLength": 36
        },
        "url": {
          "type": "string",
          "example": "url",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "UrlDepositSuccess200",
      "description": "Success"
    },
    "CEPSuccess200": {
      "type": "object",
      "required": [
        "address",
        "code",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 200
        },
        "address": {
          "type": "array",
          "items": {
            "type": "string",
            "example": {      
                "zip": "27738520",
                "street": "Rua Gabriel Passos",
                "district": "São Paulo",
                "cityState": "São Paulo"}
          }
        }
      },
      "title": "CepSuccess200",
      "description": "Success"
    },
    "UrlDepositError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 409,
        },
        "message": {
          "type": "string",
          "example": "Error",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "UrlDepositError",
      "description": "Error"
    },
    "CEPSuccess201": {
      "type": "object",
      "required": [
        "address",
        "code",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 201 
        },
        "address": {
          "type": "array",
          "items": {
            "type": "string",
            "example": {      
                "zip": "27738520",
                "street": "Rua Gabriel Passos",
                "district": "São Paulo",
                "cityState": "São Paulo"}
          }
        }
      },
      "title": "CepSuccess201",
      "description": "Success"
    },
    "LoginError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 409
        },
        "status": {
          "type": "string",
          "example": "Error",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "An error has occurred",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "LoginError",
      "description": "Error"
    },
    "CEPSuccess200": {
      "type": "object",
      "required": [
        "address",
        "code",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 200
        },
        "address": {
          "type": "array",
          "items": {
            "type": "string",
            "example": {      
                "zip": "27738520",
                "street": "Rua Gabriel Passos",
                "district": "São Paulo",
                "cityState": "São Paulo"}
          }
        }
      },
      "title": "CepSuccess200",
      "description": "Success"
    },
    "CEPSuccess201": {
      "type": "object",
      "required": [
        "address",
        "code",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 201 
        },
        "address": {
          "type": "array",
          "items": {
            "type": "string",
            "example": {      
                "zip": "27738520",
                "street": "Rua Gabriel Passos",
                "district": "São Paulo",
                "cityState": "São Paulo"}
          }
        }
      },
      "title": "CepSuccess201",
      "description": "Success"
    },
    "LoginError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 409
        },
        "status": {
          "type": "string",
          "example": "Error",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "An error has occurred",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "LoginError",
      "description": "Error"
    },
    "LoginSuccess200": {
      "type": "object",
      "required": [
        "accessToken",
        "code",
        "expiresIn",
        "status",
        "uniqueCode"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example":"Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 200
        },
        "uniqueCode": {
          "type": "string",
          "example": "TST",
          "minLength": 8,
          "maxLength": 100
        },
        "expiresIn": {
          "type": "string",
          "example": "2019-11-07 15:30:59",
          "minLength": 8,
          "maxLength": 100
        },
        "accessToken": {
          "type": "string",
          "example": "dh7878.092832s_jhs093.kij8...",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "LoginSuccess200",
      "description": "Success"
    },
    "LoginSuccess201": {
      "type": "object",
      "required": [
        "accessToken",
        "code",
        "expiresIn",
        "status",
        "uniqueCode"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example":"Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 201
        },
        "uniqueCode": {
          "type": "string",
          "example": "TST",
          "minLength": 8,
          "maxLength": 100
        },
        "expiresIn": {
          "type": "string",
          "example": "2019-11-07 15:30:59",
          "minLength": 8,
          "maxLength": 100
        },
        "accessToken": {
          "type": "string",
          "example": "dh7878.092832s_jhs093.kij8...",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "LoginSuccess201",
      "description": "Success"
    },
    "CreateUserError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 409
        },
        "status": {
          "type": "string",
          "example": "Error",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "An error has occurred",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CreateUserError",
      "description": "Error"
    },
    "CreateUserNotAcceptable": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example":406,
        },
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example":"Error description",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CreateUserNotAcceptable",
      "description": "Not-Acceptable"
    },
    "CreateUserUnauthorized": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 401
        },
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "'Email already registered",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": " CreateUserUnauthorized",
      "description": "Unauthorized"
    },
    "CPFSuccess200": {
      "type": "object",
      "required": [
        "code",
        "data",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": "200"
        },
        "message": {
          "type": "string",
          "example": "User previously approved",
          "minLength": 8,
          "maxLength": 100
        },
      },
      "title": "UserSuccess200",
      "description": "Success"
    },
    "CPFSuccess201": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": "201"
        },
        "message": {
          "type": "string",
          "example": "User previously approved",
          "minLength": 8,
          "maxLength": 100
        },
      },
      "title": "UserSuccess201",
      "description": "Success"
    },
    "TEDNotAcceptable": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example":"Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 406,
        },
        "message": {
          "type": "string",
          "example":"Error description",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "TedNotAcceptable",
      "description": "Not-Acceptable"
    },
    "Empty": {
      "type": "object",
      "title": "empty-schema"
    },
    "TEDSuccess200": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 200,
        },
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "Request accepted",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "TedSuccess200",
      "description": "Success"
    },
    "TEDSuccess201": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 201,
        },
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "Request accepted",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "TedSuccess201",
      "description": "Success"
    },
    "CPFError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 409
        },
        "message": {
          "type": "string",
          "example":"An error has occurred",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "UserError",
      "description": "Error"
    },
    "LoginUnauthorized": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 401
        },
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "Email already registered",
          "minLength": 100,
          "maxLength": 8
        }
      },
      "title": "LoginUnauthorized",
      "description": "Not Unauthorized"
    },
    "BanksSuccess200": {
      "type": "object",
      "required": [
        "code",
        "data",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 200
        },
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "data": {
          "type": "array",
          "example": [ { 
            "value": "001",
            "label": "Banco do Brasil",
            },
            { 
              "value": "237",
              "label": "Banco Bradesco"
          },
        ],
        },
      },
      "title": "BanksSuccess200",
      "description": "Success"
    },
    "BanksSuccess201": {
      "type": "object",
      "required": [
        "code",
        "data",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 201
        },
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "data": {
          "type": "array",
          "example": [ { 
            "value": "001",
            "label": "Banco do Brasil",
            },
            { 
              "value": "237",
              "label": "Banco Bradesco"
          },
        ],
        },
      },
      "title": "BanksSuccess201",
      "description": "Success"
    },
    "DataListSuccess200": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 200,
        },
        "message": {
          "type": "string",
          "example": "Request accepted",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "TransactionIdSuccess200",
      "description": "Success"
    },
    "DataListSuccess201": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 201,
        },
        "message": {
          "type": "string",
          "example": "Request accepted",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "TransactionIdSuccess201",
      "description": "Success"
    },
    "DataListNotAcceptable": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example":"Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 406
        },
        "message": {
          "type": "string",
          "example":"Error description",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "TransactionIdNotAcceptable",
      "description": "Not-Acceptable"
    },
    "CreateUserSuccess200": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 200
        },
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "Company's user successfully created",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CreateUserSuccesss200",
      "description": "Success"
    },
    "CreateUserSuccess201": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 201
        },
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "Company's user successfully created",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CreateUserSuccesss201",
      "description": "Success"
    },
    "BanksError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 409
        },
        "message": {
          "type": "string",
          "example": "An Error has occurred",
          "minLength": 8,
          "maxLength": 100
        },
        "status": {
          "type": "string",
          "example": "Error",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "BanksError",
      "description": "Error"
    },
    "CPFNotAuthentication": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 407
        },
        "message": {
          "type": "string",
          "example": "CPF not informed",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "UserNotAuthentication",
      "description": "Not-Authentication"
    },
    "Login": {
      "type": "object",
      "required": [
        "email",
        "password"
      ],
      "properties": {
        "email": {
          "type": "string",
          "example": "email@email.com",
          "minLength": 5,
          "maxLength": 50
        },
        "password": {
          "type": "string",
          "example": "12345678",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "LoginInput"
    },
    "CPFNotAcceptable": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Invalid",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 406
        },
        "message": {
          "type": "string",
          "example": "Invalid CPF number",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "UserNotAcceptable",
      "description": "Not-Acceptable"
    },
    "DataList": {
      "type": "object",
      "required": [
        "data"
      ],
      "properties": {
        "data": {
          "type": "array",
          "example": ['6786136736361', '838791629616', '32696196343314'],
          "items": {
            "type": "string",
          }
        }
      },
      "title": "DataList"
    },
    "TED": {
      "type": "object",
      "required": [
        "accountNumber",
        "accountType",
        "amount",
        "bankNumber",
        "branch",
        "digitCode",
        "idCard",
        "name"
      ],
      "properties": {
        "idCard": {
          "type": "string",
          "example": "39189183002",
          "minLength": 11,
          "maxLength": 11
        },
        "name": {
          "type": "string",
          "example": "José da Silva",
          "minLength": 5,
          "maxLength": 50
        },
        "amount": {
          "type": "integer",
          "example": "1000",
          "minimum": 500,
          "maximum": 500000,
          "exclusiveMaximum": true
        },
        "bankNumber": {
          "type": "string",
          "example": "001",
          "minLength": 3,
          "maxLength": 3
        },
        "branch": {
          "type": "integer",
          "example": "122",
          "minimum": 1,
          "maximum": 999999
        },
        "accountNumber": {
          "type": "integer",
          "example": 3582885,
          "minimum": 1,
          "maximum": 999999999
        },
        "digitCode": {
          "type": "string",
          "example": "05",
          "minLength": 1,
          "maxLength": 2
        },
        "accountType": {
          "type": "integer",
          "example": "1",
          "minimum": 1,
          "maximum": 2
        }
      },
      "title": "TedTransferInput"
    },
    "CEPError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": "409"
        },
        "status": {
          "type": "string",
          "example": "Error",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "An Error has occurred",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CepError",
      "description": "Error"
    },
    "CPF": {
      "type": "object",
      "required": [
        "id_card"
      ],
      "properties": {
        "id_card": {
          "type": "string",
          "example": "39189183002",
          "minLength": 11,
          "maxLength": 11
        }
      },
      "title": "Logininput"
    },
    "DataListError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Error",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 409
        },
        "message": {
          "type": "string",
          "example": "An error has occurred",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "TransactionIdError",
      "description": "Erro"
    },
    "LoginNotFound": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "example": 404
        },
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "message": {
          "type": "string",
          "example": "Not allowed",
          "minLength": 100,
          "maxLength": 8
        }
      },
      "title": "LoginNotFound",
      "description": "Not-Found"
    },
    "CEPNotFound": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 404
        },
        "message": {
          "type": "string",
          "example": "Invalid zip number",
          "minLength": 100,
          "maxLength": 8
        }
      },
      "title": "CepNotFound",
      "description": "Not-Found"
    },
    "CPFPostNotAuthentication": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 407,
        },
        "message": {
          "type": "string",
          "example": "CPF not informed",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CpfPostNotAuthentication",
      "description": "CPF POST Not Authentication"
    },
    "CPFPostSuccess200": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 200,
        },
        "message": {
          "type": "string",
          "example": "Request accepted",
          "minLength": 8,
          "maxLength": 100
        },
      },
      "title": "CpfPostSuccess200",
      "description": "Success"
    },
    "CPFPostSuccess201": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 201,
        },
        "message": {
          "type": "string",
          "example": "Request accepted",
          "minLength": 8,
          "maxLength": 100
        },
      },
      "title": "CpfPostSuccess201",
      "description": "Success"
    },
    "CPFPostNotAcceptable": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Invalid",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 406,
        },
        "message": {
          "type": "string",
          "example": "Invalid CPF number",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CpfPostNotAcceptable",
      "description": "CEP POST Not Acceptable"
    },
    "CPFPostError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Error",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 409,
        },
        "message": {
          "type": "string",
          "example": "An Error has occurred",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CpfPostError",
      "description": "Error"
    },
    "CPFGetSuccess200": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 200,
        },
        "message": {
          "type": "string",
          "minLength": 8,
          "maxLength": 100,
          "example": "Request accepted",
        },
      },
      "title": "CPFGetSuccess200",
      "description": "Success"
    },
    "CPFGetSuccess201": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Success",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 201,
        },
        "message": {
          "type": "string",
          "minLength": 8,
          "maxLength": 100,
          "example": "Request accepted",
        },
      },
      "title": "CPFGetSuccess201",
      "description": "Success"
    },
    "CPFGetNotAcceptable": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Invalid",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 406,
        },
        "message": {
          "type": "string",
          "example": "Error description",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CpfGetNotAcceptable",
      "description": "CEP GET Not Acceptable"
    },
    "CPFGetNotAuthentication": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 407,
        },
        "message": {
          "type": "string",
          "example": "CPF not informed",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CpfGetNotAuthentication",
      "description": "CPF GET Not Authentication"
    },
    "CPFGetError": {
      "type": "object",
      "required": [
        "code",
        "message",
        "status"
      ],
      "properties": {
        "status": {
          "type": "string",
          "example": "Fail",
          "minLength": 8,
          "maxLength": 100
        },
        "code": {
          "type": "integer",
          "example": 409,
        },
        "message": {
          "type": "string",
          "example": "An error has occurred",
          "minLength": 8,
          "maxLength": 100
        }
      },
      "title": "CpfGetError",
      "description": "Error"
    },
  },
}

  
const options = {
  swaggerDefinition,
  // Paths to files containing OpenAPI definitions
  apis: ['app.js'],
};


const swaggerSpec = swaggerJSDoc(options);
app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.listen(8080, '127.0.0.1', () => {
    console.log(`Server listening on port ${port}`);
  });
