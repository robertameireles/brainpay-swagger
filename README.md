# BrainPay Swagger

This project was developer on node.js

```bash
yarn install
```

In the project directory (app.js), you can run: 
### `nodemon -L app.js`

The project will be running:
### `localhost\8080`



**If you need to change any swagger items:**

1. Open API Gateway.

2. Make your changes in API Gateway.

3. Make Deploy API:

    * Actions / Deploy API;

4. Import json from aws:

    * Documentation / Publish Documentation;

    * Enter the version number;

    * Choose Export as Swagger and JSON;

    * Copy the part changed to include in swaggerDefinition method in app.js;

    * See the file models.js to make the changes that are 
    not provided in the aws json. The aws doesn't accept the 
    information of example.

    * If you are including a new endpoint, include it in models for backup 
    and after that include it in swaggerDefinition method in app.js.
    Is important there are the same name in model (definitions) 
    and in schema "$ref" (Responses) for example:

    - DEFINITIONS:
    "LoginSuccess200": {
         ...
    }

    - RESPONSES:
    "responses": {
          "200": {
            "description": "200 response",
            "schema": {
              "$ref": "#/definitions/LoginSuccess200"
            },
    
    * Options, pagsmile and payload are not necessary in swagger, it only exist in aws.

    * Note: externalDocs doesn't exist in aws. It information is below "info":

      "externalDocs": {
        "description": "BrainPay Website",
        "url": "https://www.brainpay.com.br"
      },

      - Example:

      const swaggerDefinition = {
        "swagger": "2.0",
        "info": {
            "description": "BrainPay API",
            "version": "2021-05-11T14:16:46Z",
            "title": "BrainPay API"
        },
        "externalDocs": {
            "description": "BrainPay Website",
            "url": "https://www.brainpay.com.br"
        },
        ...
