/*Replace the definitions item with the json below, because 
the json below shows examples of the models: */

"definitions": {
  "CEPSuccess200": {
    "type": "object",
    "required": [
      "address",
      "code",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 200
      },
      "address": {
        "type": "array",
        "items": {
          "type": "string",
          "example": {      
              "zip": "27738520",
              "street": "Rua Gabriel Passos",
              "district": "São Paulo",
              "city_state": "São Paulo"}
        }
      }
    },
    "title": "CepSuccess200",
    "description": "Success"
  },
  "CEPSuccess201": {
    "type": "object",
    "required": [
      "address",
      "code",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 201 
      },
      "address": {
        "type": "array",
        "items": {
          "type": "string",
          "example": {      
              "zip": "27738520",
              "street": "Rua Gabriel Passos",
              "district": "São Paulo",
              "city_state": "São Paulo"}
        }
      }
    },
    "title": "CepSuccess201",
    "description": "Success"
  },
  "LoginError": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 409
      },
      "status": {
        "type": "string",
        "example": "Error",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "An error has occurred",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "LoginError",
    "description": "Error"
  },
  "LoginSuccess200": {
    "type": "object",
    "required": [
      "accessToken",
      "code",
      "expiresIn",
      "status",
      "uniqueCode"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example":"Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 200
      },
      "uniqueCode": {
        "type": "string",
        "example": "TST",
        "minLength": 8,
        "maxLength": 100
      },
      "expiresIn": {
        "type": "string",
        "example": "2019-11-07 15:30:59",
        "minLength": 8,
        "maxLength": 100
      },
      "accessToken": {
        "type": "string",
        "example": "dh7878.092832s_jhs093.kij8...",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "LoginSuccess200",
    "description": "Success"
  },
  "LoginSuccess201": {
    "type": "object",
    "required": [
      "accessToken",
      "code",
      "expiresIn",
      "status",
      "uniqueCode"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example":"Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 201
      },
      "uniqueCode": {
        "type": "string",
        "example": "TST",
        "minLength": 8,
        "maxLength": 100
      },
      "expiresIn": {
        "type": "string",
        "example": "2019-11-07 15:30:59",
        "minLength": 8,
        "maxLength": 100
      },
      "accessToken": {
        "type": "string",
        "example": "dh7878.092832s_jhs093.kij8...",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "LoginSuccess201",
    "description": "Success"
  },
  "CreateUserError": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 409
      },
      "status": {
        "type": "string",
        "example": "Error",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "An error has occurred",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CreateUserError",
    "description": "Error"
  },
  "CreateUserNotAcceptable": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example":406,
      },
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example":"Error description",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CreateUserNotAcceptable",
    "description": "Not-Acceptable"
  },
  "CreateUserUnauthorized": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 401
      },
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "'Email already registered",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": " CreateUserUnauthorized",
    "description": "Unauthorized"
  },
  "CPFSuccess200": {
    "type": "object",
    "required": [
      "code",
      "data",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": "200"
      },
      "message": {
        "type": "string",
        "example": "User previously approved",
        "minLength": 8,
        "maxLength": 100
      },
    },
    "title": "UserSuccess200",
    "description": "Success"
  },
  "CPFSuccess201": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": "201"
      },
      "message": {
        "type": "string",
        "example": "User previously approved",
        "minLength": 8,
        "maxLength": 100
      },
    },
    "title": "UserSuccess201",
    "description": "Success"
  },
  "TEDNotAcceptable": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example":"Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 406,
      },
      "message": {
        "type": "string",
        "example":"Error description",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "TedNotAcceptable",
    "description": "Not-Acceptable"
  },
  "Empty": {
    "type": "object",
    "title": "empty-schema"
  },
  "TEDSuccess200": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 200,
      },
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "Request accepted",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "TedSuccess200",
    "description": "Success"
  },
  "TEDSuccess201": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 201,
      },
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "Request accepted",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "TedSuccess201",
    "description": "Success"
  },
  "CPFError": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 409
      },
      "message": {
        "type": "string",
        "example":"An error has occurred",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "UserError",
    "description": "Error"
  },
  "LoginUnauthorized": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 401
      },
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "Email already registered",
        "minLength": 100,
        "maxLength": 8
      }
    },
    "title": "LoginUnauthorized",
    "description": "Not Unauthorized"
  },
  "BanksSuccess200": {
    "type": "object",
    "required": [
      "code",
      "data",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 200
      },
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "data": {
        "type": "array",
        "example": [ { 
          "value": "001",
          "label": "Banco do Brasil",
          },
          { 
            "value": "237",
            "label": "Banco Bradesco"
        },
      ],
      },
    },
    "title": "BanksSuccess200",
    "description": "Success"
  },
  "BanksSuccess201": {
    "type": "object",
    "required": [
      "code",
      "data",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 201
      },
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "data": {
        "type": "array",
        "example": [ { 
          "value": "001",
          "label": "Banco do Brasil",
          },
          { 
            "value": "237",
            "label": "Banco Bradesco"
        },
      ],
      },
    },
    "title": "BanksSuccess201",
    "description": "Success"
  },
  "DataListSuccess200": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 200,
      },
      "message": {
        "type": "string",
        "example": "Request accepted",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "TransactionIdSuccess200",
    "description": "Success"
  },
  "DataListSuccess201": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 201,
      },
      "message": {
        "type": "string",
        "example": "Request accepted",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "TransactionIdSuccess201",
    "description": "Success"
  },
  "DataListNotAcceptable": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example":"Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 406
      },
      "message": {
        "type": "string",
        "example":"Error description",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "TransactionIdNotAcceptable",
    "description": "Not-Acceptable"
  },
  "CreateUserSuccess200": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 200
      },
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "Company's user successfully created",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CreateUserSuccesss200",
    "description": "Success"
  },
  "CreateUserSuccess201": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 201
      },
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "Company's user successfully created",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CreateUserSuccesss201",
    "description": "Success"
  },
  "BanksError": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 409
      },
      "message": {
        "type": "string",
        "example": "An Error has occurred",
        "minLength": 8,
        "maxLength": 100
      },
      "status": {
        "type": "string",
        "example": "Error",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "BanksError",
    "description": "Error"
  },
  "CPFNotAuthentication": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 407
      },
      "message": {
        "type": "string",
        "example": "CPF not informed",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "UserNotAuthentication",
    "description": "Not-Authentication"
  },
  "Login": {
    "type": "object",
    "required": [
      "email",
      "password"
    ],
    "properties": {
      "email": {
        "type": "string",
        "example": "email@email.com",
        "minLength": 5,
        "maxLength": 50
      },
      "password": {
        "type": "string",
        "example": "12345678",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "LoginInput"
  },
  "CPFNotAcceptable": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Invalid",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 406
      },
      "message": {
        "type": "string",
        "example": "Invalid CPF number",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "UserNotAcceptable",
    "description": "Not-Acceptable"
  },
  "DataList": {
    "type": "object",
    "required": [
      "data"
    ],
    "properties": {
      "data": {
        "type": "array",
        "example": ['6786136736361', '838791629616', '32696196343314'],
        "items": {
          "type": "string",
        }
      }
    },
    "title": "DataList"
  },
  "TED": {
    "type": "object",
    "required": [
      "accountNumber",
      "accountType",
      "amount",
      "bankNumber",
      "branch",
      "digitCode",
      "idCard",
      "name"
    ],
    "properties": {
      "idCard": {
        "type": "string",
        "example": "39189183002",
        "minLength": 11,
        "maxLength": 11
      },
      "name": {
        "type": "string",
        "example": "José da Silva",
        "minLength": 5,
        "maxLength": 50
      },
      "amount": {
        "type": "integer",
        "example": "1000",
        "minimum": 500,
        "maximum": 500000,
        "exclusiveMaximum": true
      },
      "bankNumber": {
        "type": "string",
        "example": "001",
        "minLength": 3,
        "maxLength": 3
      },
      "branch": {
        "type": "integer",
        "example": "122",
        "minimum": 1,
        "maximum": 999999
      },
      "accountNumber": {
        "type": "integer",
        "example": "03582885",
        "minimum": 1,
        "maximum": 999999999
      },
      "digitCode": {
        "type": "string",
        "example": "05",
        "minLength": 1,
        "maxLength": 2
      },
      "accountType": {
        "type": "integer",
        "example": "1",
        "minimum": 1,
        "maximum": 2
      }
    },
    "title": "TedTransferInput"
  },
  "CEPError": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": "409"
      },
      "status": {
        "type": "string",
        "example": "Error",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "An Error has occurred",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CepError",
    "description": "Error"
  },
  "CPF": {
    "type": "object",
    "required": [
      "id_card"
    ],
    "properties": {
      "id_card": {
        "type": "string",
        "example": "39189183002",
        "minLength": 11,
        "maxLength": 11
      }
    },
    "title": "Logininput"
  },
  "DataListError": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Error",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 409
      },
      "message": {
        "type": "string",
        "example": "An error has occurred",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "TransactionIdError",
    "description": "Erro"
  },
  "LoginNotFound": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "code": {
        "type": "integer",
        "example": 404
      },
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "message": {
        "type": "string",
        "example": "Not allowed",
        "minLength": 100,
        "maxLength": 8
      }
    },
    "title": "LoginNotFound",
    "description": "Not-Found"
  },
  "CEPNotFound": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 404
      },
      "message": {
        "type": "string",
        "example": "Invalid zip number",
        "minLength": 100,
        "maxLength": 8
      }
    },
    "title": "CepNotFound",
    "description": "Not-Found"
  },
  "CPFPostNotAuthentication": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 407,
      },
      "message": {
        "type": "string",
        "example": "CPF not informed",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CpfPostNotAuthentication",
    "description": "CPF POST Not Authentication"
  },
  "CPFPostSuccess200": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 200,
      },
      "message": {
        "type": "string",
        "example": "Request accepted",
        "minLength": 8,
        "maxLength": 100
      },
    },
    "title": "CpfPostSuccess200",
    "description": "Success"
  },
  "CPFPostSuccess201": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 201,
      },
      "message": {
        "type": "string",
        "example": "Request accepted",
        "minLength": 8,
        "maxLength": 100
      },
    },
    "title": "CpfPostSuccess201",
    "description": "Success"
  },
  "CPFPostNotAcceptable": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Invalid",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 406,
      },
      "message": {
        "type": "string",
        "example": "Invalid CPF number",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CpfPostNotAcceptable",
    "description": "CEP POST Not Acceptable"
  },
  "CPFPostError": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Error",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 409,
      },
      "message": {
        "type": "string",
        "example": "An Error has occurred",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CpfPostError",
    "description": "Error"
  },
  "CPFGetSuccess200": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 200,
      },
      "message": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "example": "Request accepted",
      },
    },
    "title": "CPFGetSuccess200",
    "description": "Success"
  },
  "CPFGetSuccess201": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Success",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 201,
      },
      "message": {
        "type": "string",
        "minLength": 8,
        "maxLength": 100,
        "example": "Request accepted",
      },
    },
    "title": "CPFGetSuccess201",
    "description": "Success"
  },
  "CPFGetNotAcceptable": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Invalid",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 406,
      },
      "message": {
        "type": "string",
        "example": "Error description",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CpfGetNotAcceptable",
    "description": "CEP GET Not Acceptable"
  },
  "CPFGetNotAuthentication": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 407,
      },
      "message": {
        "type": "string",
        "example": "CPF not informed",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CpfGetNotAuthentication",
    "description": "CPF GET Not Authentication"
  },
  "CPFGetError": {
    "type": "object",
    "required": [
      "code",
      "message",
      "status"
    ],
    "properties": {
      "status": {
        "type": "string",
        "example": "Fail",
        "minLength": 8,
        "maxLength": 100
      },
      "code": {
        "type": "integer",
        "example": 409,
      },
      "message": {
        "type": "string",
        "example": "An error has occurred",
        "minLength": 8,
        "maxLength": 100
      }
    },
    "title": "CpfGetError",
    "description": "Error"
  },
}

